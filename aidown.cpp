
#include "aidown.h"
#include<stdlib.h>
#include<time.h>
AIdown::AIdown()
{

}
bool AIdown::peopleDown(int (*p)[15], int person,Point point)
{
    if(p[point.x][point.y]!=0)
        return false;
    else
    {
        p[point.x][point.y]=person;
        return true;
    }
}

int AIdown::judge(int (*p)[15],Point pointOne)
{
    int win=0;
    if(p[pointOne.x][pointOne.y]==1)
    {
        for(int i=pointOne.x;i<15;i++)//right
        {
            if(p[i][pointOne.y]==1)
                win++;
            if(p[i][pointOne.y]==2||p[i][pointOne.y]==0)
                break;
        }
        for(int i=pointOne.x;i>-1;i--)//left
        {
            if(p[i][pointOne.y]==1)
                win++;
            if(p[i][pointOne.y]==2||p[i][pointOne.y]==0)
                break;
        }
        if(win>5)
            return 1;
        win=0;
        for(int j=pointOne.y;j<15;j++)//down
        {
            if(p[pointOne.x][j]==1)
                win++;
            if(p[pointOne.x][j]==0||p[pointOne.x][j]==2)
                break;
        }
        for(int j=pointOne.y;j>-1;j--)//up
        {
            if(p[pointOne.x][j]==1)
                win++;
            if(p[pointOne.x][j]==0||p[pointOne.x][j]==2)
                break;
        }
        if(win>5)
            return 1;
        win=0;
        for(int i=pointOne.x,j=pointOne.y;i<15&&j<15;i++,j++)//right-down
        {
            if(p[i][j]==1)
                win++;
            if(p[i][j]==0||p[i][j]==2)
                break;
        }
        for(int i=pointOne.x,j=pointOne.y;i>-1&&j>-1;i--,j--)//right-down
        {
            if(p[i][j]==1)
                win++;
            if(p[i][j]==0||p[i][j]==2)
                break;
        }
        if(win>5)
            return 1;
        win=0;
        for(int i=pointOne.x,j=pointOne.y;i>-1&&j<15;i--,j++)//right-down
        {
            if(p[i][j]==1)
                win++;
            if(p[i][j]==0||p[i][j]==2)
                break;
        }
        for(int i=pointOne.x,j=pointOne.y;i<15&&j>-1;i++,j--)//right-down
        {
            if(p[i][j]==1)
                win++;
            if(p[i][j]==0||p[i][j]==2)
                break;
        }
        if(win>5)
            return 1;
        win=0;
    }
    if(p[pointOne.x][pointOne.y]==2)
    {
        for(int i=pointOne.x;i<15;i++)//right
        {
            if(p[i][pointOne.y]==2)
                win++;
            if(p[i][pointOne.y]==1||p[i][pointOne.y]==0)
                break;
        }
        for(int i=pointOne.x;i>-1;i--)//left
        {
            if(p[i][pointOne.y]==2)
                win++;
            if(p[i][pointOne.y]==1||p[i][pointOne.y]==0)
                break;
        }
        if(win>5)
            return 2;
        win=0;
        for(int j=pointOne.y;j<15;j++)//down
        {
            if(p[pointOne.x][j]==2)
                win++;
            if(p[pointOne.x][j]==0||p[pointOne.x][j]==1)
                break;
        }
        for(int j=pointOne.y;j>-1;j--)//up
        {
            if(p[pointOne.x][j]==2)
                win++;
            if(p[pointOne.x][j]==0||p[pointOne.x][j]==1)
                break;
        }
        if(win>5)
            return 2;
        win=0;
        for(int i=pointOne.x,j=pointOne.y;i<15&&j<15;i++,j++)//right-down
        {
            if(p[i][j]==2)
                win++;
            if(p[i][j]==0||p[i][j]==1)
                break;
        }
        for(int i=pointOne.x,j=pointOne.y;i>-1&&j>-1;i--,j--)//right-down
        {
            if(p[i][j]==2)
                win++;
            if(p[i][j]==0||p[i][j]==1)
                break;
        }
        if(win>5)
            return 2;
        win=0;
        for(int i=pointOne.x,j=pointOne.y;i>-1&&j<15;i--,j++)//right-down
        {
            if(p[i][j]==2)
                win++;
            if(p[i][j]==0||p[i][j]==1)
                break;
        }
        for(int i=pointOne.x,j=pointOne.y;i<15&&j>-1;i++,j--)//right-down
        {
            if(p[i][j]==2)
                win++;
            if(p[i][j]==0||p[i][j]==1)
                break;
        }
        if(win>5)
            return 2;
        win=0;
    }
    return 0;
}

Point AIdown::computer(int (*p)[15],int me,Point point)
{
    int value=0,max=0,num=0,it=3-me;struct Point temp={0,0};
    struct Point* ptemp=(struct Point*)malloc(100*sizeof(struct Point));
    if(ptemp==NULL)
        exit(0);
    for(int x=0;x<15;x++)
    {
        for(int y=0;y<15;y++)
        {
            if(p[x][y]==0)
            {
                int t=0;
                for(int i=x+1;i<15;i++)
                {

                    if(p[i][y]==it)
                    {
                        value+=5+5*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][y]==me||p[i][y]==0)
                    {
                        if(p[i][y]==me&&t>=2)
                            value-=30;
                        break;
                    }
                }
                for(int i=x-1;i>-1;i--)
                {

                    if(p[i][y]==it)
                    {
                        value+=5+5*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][y]==me||p[i][y]==0)
                    {
                        if(p[i][y]==me&&t>=2)
                            value-=30;
                        break;
                    }
                }
                t=0;
                for(int j=y+1;j<15;j++)
                {

                    if(p[x][j]==it)
                    {
                        value+=5+5*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==me)
                    {
                        if(p[x][j]==me&&t>=2)
                            value-=30;
                        break;
                    }
                }
                for(int j=y-1;j>-1;j--)
                {

                    if(p[x][j]==it)
                    {
                        value+=5+5*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==me)
                    {
                        if(p[x][j]==me&&t>=2)
                            value-=30;
                        break;
                    }
                }
                t=0;
                for(int i=x+1,j=y+1;i<15&&j<15;i++,j++)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+7*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=30;
                        break;
                    }
                }
                for(int i=x-1,j=y-1;i>-1&&j>-1;i--,j--)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+7*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=30;
                        break;
                    }
                }
                t=0;
                for(int i=x-1,j=y+1;i>-1&&j<15;i--,j++)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+7*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=30;
                        break;
                    }
                }
                for(int i=x+1,j=y-1;i<15&&j>-1;i++,j--)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+7*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=30;
                        break;
                    }
                }



                t=0;
                for(int i=x+1;i<15;i++)//right
                {

                    if(p[i][y]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][y]==it||p[i][y]==0)
                    {
                        if(p[i][y]==it&&t>=2)
                            value-=15;
                        break;
                    }
                }
                for(int i=x-1;i>-1;i--)//left
                {
                    if(p[i][y]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][y]==it||p[i][y]==0)
                    {
                        if(p[i][y]==it&&t>=2)
                            value-=15;
                        break;
                    }
                }
                t=0;
                for(int j=y+1;j<15;j++)//down
                {
                    if(p[x][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==it)
                    {
                        if(p[x][j]==it&&t>=2)
                            value-=15;
                        break;
                    }
                }
                for(int j=y-1;j>-1;j--)//up
                {
                    if(p[x][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==it)
                    {
                        if(p[x][j]==it&&t>=2)
                            value-=15;
                        break;
                    }
                }
                t=0;
                for(int i=x+1,j=y+1;i<15&&j<15;i++,j++)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                            value-=15;
                        break;
                    }
                }
                for(int i=x-1,j=y-1;i>-1&&j>-1;i--,j--)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                            value-=15;
                        break;
                    }
                }
                t=0;
                for(int i=x-1,j=y+1;i>-1&&j<15;i--,j++)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                            value-=15;
                        break;
                    }
                }
                for(int i=x+1,j=y-1;i<15&&j>-1;i++,j--)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                            value-=15;
                        break;
                    }
                }
                if(value>max)
                {
                    max=value;
                    temp.x=x;temp.y=y;
                    ptemp[0].x=x;
                    ptemp[0].y=y;
                    num=0;
                }
                if(value==max&&num<98)
                {
                    num++;
                    ptemp[num].x=x;
                    ptemp[num].y=y;
                }
                value=0;
                t=0;
            }
        }
    }
    int t=0;
    for(int i=0;i<15;i++)
    {
        for(int j=0;j<15;j++)
        {
            if(p[i][j]!=0)
            {
                t=1;
                break;
            }
        }
        if(t)
            break;
    }
    if(t==0)
    {
        num=0;
        temp.x=7;
        temp.y=7;
    }
    if(num==0)
    {
        p[temp.x][temp.y]=me;
        point.x=temp.x;point.y=temp.y;
    }
    else
    {
        srand((unsigned)time(NULL));
        int s=rand()%(num+1);
        p[ptemp[s].x][ptemp[s].y]=me;
        point.x=ptemp[s].x;point.y=ptemp[s].y;
    }
    free(ptemp);
    return point;
}
Point AIdown::computerTry(int (*p)[15],int me,Point point)
{
    int value=0,max=0,num=0,it=3-me;struct Point temp={0,0};
    struct Point* ptemp=(struct Point*)malloc(100*sizeof(struct Point));
    if(ptemp==NULL)
        exit(0);
    for(int x=0;x<15;x++)
    {
        for(int y=0;y<15;y++)
        {
            if(p[x][y]==0)
            {
                int t=0;
                for(int i=x+1;i<15;i++)
                {

                    if(p[i][y]==it)
                    {
                        value+=5+5*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][y]==me||p[i][y]==0)
                    {
                        if(p[i][y]==me&&t>=2)
                            value-=30;
                        if(p[i][y]==0)
                            value+=3;
                        break;
                    }
                }
                for(int i=x-1;i>-1;i--)
                {

                    if(p[i][y]==it)
                    {
                        value+=5+5*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][y]==me||p[i][y]==0)
                    {
                        if(p[i][y]==me&&t>=2)
                            value-=30;
                        if(p[i][y]==0)
                            value+=3;
                        break;
                    }
                }
                t=0;
                for(int j=y+1;j<15;j++)
                {

                    if(p[x][j]==it)
                    {
                        value+=5+5*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==me)
                    {
                        if(p[x][j]==me&&t>=2)
                            value-=30;
                        if(p[x][j]==0)
                            value+=3;
                        break;
                    }
                }
                for(int j=y-1;j>-1;j--)
                {

                    if(p[x][j]==it)
                    {
                        value+=5+5*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==me)
                    {
                        if(p[x][j]==me&&t>=2)
                            value-=30;
                        if(p[x][j]==0)
                            value+=3;
                        break;
                    }
                }
                t=0;
                for(int i=x+1,j=y+1;i<15&&j<15;i++,j++)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+7*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=30;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }
                for(int i=x-1,j=y-1;i>-1&&j>-1;i--,j--)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+7*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=30;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }
                t=0;
                for(int i=x-1,j=y+1;i>-1&&j<15;i--,j++)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+7*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=30;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }
                for(int i=x+1,j=y-1;i<15&&j>-1;i++,j--)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+7*t;
                        if(t>=2)
                            value+=30;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=30;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }



                t=0;
                for(int i=x+1;i<15;i++)//right
                {

                    if(p[i][y]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][y]==it||p[i][y]==0)
                    {
                        if(p[i][y]==it&&t>=2)
                            value-=15;
                        if(p[i][y]==0)
                            value+=4;
                        break;
                    }
                }
                for(int i=x-1;i>-1;i--)//left
                {
                    if(p[i][y]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][y]==it||p[i][y]==0)
                    {
                        if(p[i][y]==it&&t>=2)
                            value-=15;
                        if(p[i][y]==0)
                            value+=4;
                        break;
                    }
                }
                t=0;
                for(int j=y+1;j<15;j++)//down
                {
                    if(p[x][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==it)
                    {
                        if(p[x][j]==it&&t>=2)
                            value-=15;
                        if(p[x][j]==0)
                            value+=4;
                        break;
                    }
                }
                for(int j=y-1;j>-1;j--)//up
                {
                    if(p[x][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==it)
                    {
                        if(p[x][j]==it&&t>=2)
                            value-=15;
                        if(p[x][j]==0)
                            value+=4;
                        break;
                    }
                }
                t=0;
                for(int i=x+1,j=y+1;i<15&&j<15;i++,j++)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                            value-=15;
                        if(p[i][j]==0)
                            value+=4;
                        break;
                    }
                }
                for(int i=x-1,j=y-1;i>-1&&j>-1;i--,j--)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                            value-=15;
                        if(p[i][j]==0)
                            value+=4;
                        break;
                    }
                }
                t=0;
                for(int i=x-1,j=y+1;i>-1&&j<15;i--,j++)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                            value-=15;
                        if(p[i][j]==0)
                            value+=4;
                        break;
                    }
                }
                for(int i=x+1,j=y-1;i<15&&j>-1;i++,j--)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=20;
                        if(t>=3)
                            value+=100;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                            value-=15;
                        if(p[i][j]==0)
                            value+=4;
                        break;
                    }
                }
                if(value>max)
                {
                    max=value;
                    temp.x=x;temp.y=y;
                    ptemp[0].x=x;
                    ptemp[0].y=y;
                    num=0;
                }
                if(value==max&&num<98)
                {
                    num++;
                    ptemp[num].x=x;
                    ptemp[num].y=y;
                }
                value=0;
                t=0;
            }
        }
    }
    int t=0;
    for(int i=0;i<15;i++)
    {
        for(int j=0;j<15;j++)
        {
            if(p[i][j]!=0)
            {
                t=1;
                break;
            }
        }
        if(t)
            break;
    }
    if(t==0)
    {
        num=0;
        temp.x=7;
        temp.y=7;
    }
    if(num==0)
    {
        p[temp.x][temp.y]=me;
        point.x=temp.x;point.y=temp.y;
    }
    else
    {
        srand((unsigned)time(NULL));
        int s=rand()%(num+1);
        p[ptemp[s].x][ptemp[s].y]=me;
        point.x=ptemp[s].x;point.y=ptemp[s].y;
    }
    free(ptemp);
    return point;
}
Point AIdown::computerThree(int (*p)[15], int me, Point pointOne)
{
    int value=0,max=0,num=0,it=3-me;struct Point temp={0,0};
    struct Point* ptemp=(struct Point*)malloc(100*sizeof(struct Point));
    if(ptemp==NULL)
        exit(0);
    for(int x=0;x<15;x++)
    {
        for(int y=0;y<15;y++)
        {
            if(p[x][y]==0)
            {
                int t=0,q=0;
                for(int i=x+1;i<15;i++)
                {

                    if(p[i][y]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][y]==me||p[i][y]==0)
                    {
                        if(p[i][y]==me&&t>=2)
                            value-=40;
                        if(p[i][y]==0)
                            value+=3;
                        break;
                    }
                }
                for(int i=x-1;i>-1;i--)
                {

                    if(p[i][y]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][y]==me||p[i][y]==0)
                    {
                        if(p[i][y]==me&&t>=2)
                            value-=40;
                        if(p[i][y]==0)
                            value+=3;
                        break;
                    }
                }
                t=0;
                for(int j=y+1;j<15;j++)
                {

                    if(p[x][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==me)
                    {
                        if(p[x][j]==me&&t>=2)
                            value-=40;
                        if(p[x][j]==0)
                            value+=3;
                        break;
                    }
                }
                for(int j=y-1;j>-1;j--)
                {

                    if(p[x][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==me)
                    {
                        if(p[x][j]==me&&t>=2)
                            value-=40;
                        if(p[x][j]==0)
                            value+=3;
                        break;
                    }
                }
                t=0;
                for(int i=x+1,j=y+1;i<15&&j<15;i++,j++)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=40;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }
                for(int i=x-1,j=y-1;i>-1&&j>-1;i--,j--)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=40;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }
                t=0;
                for(int i=x-1,j=y+1;i>-1&&j<15;i--,j++)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=40;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }
                for(int i=x+1,j=y-1;i<15&&j>-1;i++,j--)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=40;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }





                t=0;q=0;
                if(x+1>=15)
                    value-=15;
                for(int i=x+1;i<15;i++)//right
                {

                    if(p[i][y]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][y]==it||p[i][y]==0)
                    {
                        if(p[i][y]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][y]==0)
                            value+=4;
                        if(p[i][y]==0&&(i+2)<15&&p[i+1][y]==me&&p[i+2][y]!=1)
                            value+=5;
                        break;
                    }
                }
                if(x-1<0)
                    value-=15;
                for(int i=x-1;i>-1;i--)//left
                {
                    if(p[i][y]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][y]==it||p[i][y]==0)
                    {
                        if(p[i][y]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][y]==0)
                            value+=4;
                        if(p[i][y]==0&&(i-2)>0&&p[i-1][y]==me&&p[i-2][y]!=1)
                            value+=5;
                        break;
                    }
                }
                if(q==0&&t>=2)
                    value+=30;
                t=0;q=0;
                if(y+1>=15)
                    value-=15;
                for(int j=y+1;j<15;j++)//down
                {
                    if(p[x][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==it)
                    {
                        if(p[x][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[x][j]==0)
                            value+=4;
                        if(p[x][j]==0&&(j+2)<=15&&p[x][j+1]==me&&p[x][j+2]!=1)
                            value+=5;
                        break;
                    }
                }
                if(y-1<0)
                    value-=15;
                for(int j=y-1;j>-1;j--)//up
                {
                    if(p[x][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==it)
                    {
                        if(p[x][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[x][j]==0)
                            value+=4;
                        if(p[x][j]==0&&(j-2)>0&&p[x][j-1]==me&&p[x][j-2]!=1)
                            value+=5;
                        break;
                    }
                }
                if(q==0&&t>=2)
                    value+=30;
                t=0;q=0;
                if(x+1>=15||y+1>=15)
                    value-=15;
                for(int i=x+1,j=y+1;i<15&&j<15;i++,j++)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][j]==0)
                            value+=4;
                        if(p[i][j]==0&&(j+2)<15&&(i+2)<15&&p[i+1][j+1]==me\
                        &&p[i+2][j+2]!=1)
                            value+=5;
                        break;
                    }
                }
                for(int i=x-1,j=y-1;i>-1&&j>-1;i--,j--)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][j]==0)
                            value+=4;
                        if(p[i][j]==0&&(j-2)>0&&(i-2)>0&&p[i-1][j-1]==me\
                        &&p[i-2][j-2]!=1)
                            value+=5;
                        break;
                    }
                }
                if(q==0&&t>=2)
                    value+=30;
                t=0;q=0;
                if(x-1<0||y+1>=15)
                    value-=15;
                for(int i=x-1,j=y+1;i>-1&&j<15;i--,j++)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][j]==0)
                            value+=4;
                        if(p[i][j]==0&&(j+2)<15&&(i-2)>0&&p[i-1][j+1]==me\
                        &&p[i-2][j+2]!=1)
                            value+=5;
                        break;
                    }
                }
                for(int i=x+1,j=y-1;i<15&&j>-1;i++,j--)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==me&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][j]==0)
                            value+=4;
                        if(p[i][j]==0&&(j-2)>0&&(i+2)<15&&p[i+1][j-1]==me\
                        &&p[i+2][j-2]!=1)
                            value+=5;
                        break;
                    }
                }
                if(q==0&&t>=2)
                    value+=30;
                t=0;q=0;
                if(value>max)
                {
                    max=value;
                    temp.x=x;temp.y=y;
                    ptemp[0].x=x;
                    ptemp[0].y=y;
                    num=0;
                }
                if(value==max&&num<98)
                {
                    num++;
                    ptemp[num].x=x;
                    ptemp[num].y=y;
                }
                value=0;
                t=0;
            }
        }
    }
    int t=0;
    for(int i=0;i<15;i++)
    {
        for(int j=0;j<15;j++)
        {
            if(p[i][j]!=0)
            {
                t=1;
                break;
            }
        }
        if(t)
            break;
    }
    if(t==0)
    {
        num=0;
        temp.x=7;
        temp.y=7;
    }
    if(num==0)
    {
        p[temp.x][temp.y]=me;
        pointOne.x=temp.x;pointOne.y=temp.y;
    }
    else
    {
        srand((unsigned)time(NULL));
        int s=rand()%(num+1);
        p[ptemp[s].x][ptemp[s].y]=me;
        pointOne.x=ptemp[s].x;pointOne.y=ptemp[s].y;
    }
    free(ptemp);
    return pointOne;
}
Point AIdown::computerThreeTwo(int (*p)[15],int me,Point point)
{
    int value=0,max=0,num=0,it=3-me;struct Point temp={0,0};
    struct Point* ptemp=(struct Point*)malloc(100*sizeof(struct Point));
    if(ptemp==NULL)
        exit(0);
    for(int x=0;x<15;x++)
    {
        for(int y=0;y<15;y++)
        {
            if(p[x][y]==0)
            {
                int t=0,q=0;
                for(int i=x+1;i<15;i++)
                {

                    if(p[i][y]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][y]==me||p[i][y]==0)
                    {
                        if(p[i][y]==me&&t>=2)
                            value-=40;
                        if(p[i][y]==0)
                            value+=3;
                        break;
                    }
                }
                for(int i=x-1;i>-1;i--)
                {

                    if(p[i][y]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][y]==me||p[i][y]==0)
                    {
                        if(p[i][y]==me&&t>=2)
                            value-=40;
                        if(p[i][y]==0)
                            value+=3;
                        break;
                    }
                }
                t=0;
                for(int j=y+1;j<15;j++)
                {

                    if(p[x][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==me)
                    {
                        if(p[x][j]==me&&t>=2)
                            value-=40;
                        if(p[x][j]==0)
                            value+=3;
                        break;
                    }
                }
                for(int j=y-1;j>-1;j--)
                {

                    if(p[x][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==me)
                    {
                        if(p[x][j]==me&&t>=2)
                            value-=40;
                        if(p[x][j]==0)
                            value+=3;
                        break;
                    }
                }
                t=0;
                for(int i=x+1,j=y+1;i<15&&j<15;i++,j++)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=40;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }
                for(int i=x-1,j=y-1;i>-1&&j>-1;i--,j--)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=40;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }
                t=0;
                for(int i=x-1,j=y+1;i>-1&&j<15;i--,j++)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=40;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }
                for(int i=x+1,j=y-1;i<15&&j>-1;i++,j--)
                {

                    if(p[i][j]==it)
                    {
                        value+=5+6*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=50;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==me)
                    {
                        if(p[i][j]==me&&t>=2)
                            value-=40;
                        if(p[i][j]==0)
                            value+=3;
                        break;
                    }
                }





                t=0;q=0;
                if(x+1>=15)
                    value-=15;
                for(int i=x+1;i<15;i++)//right
                {

                    if(p[i][y]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][y]==it||p[i][y]==0)
                    {
                        if(p[i][y]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][y]==0)
                            value+=4;
                        if(p[i][y]==0&&(i+2)<15&&p[i+1][y]==me&&p[i+2][y]!=1)
                            value+=5;
                        break;
                    }
                }
                if(x-1<0)
                    value-=15;
                for(int i=x-1;i>-1;i--)//left
                {
                    if(p[i][y]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][y]==it||p[i][y]==0)
                    {
                        if(p[i][y]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][y]==0)
                            value+=4;
                        if(p[i][y]==0&&(i-2)>0&&p[i-1][y]==me&&p[i-2][y]!=1)
                            value+=5;
                        break;
                    }
                }
                if(q==0&&t>=2)
                    value+=30;
                t=0;q=0;
                if(y+1>=15)
                    value-=15;
                for(int j=y+1;j<15;j++)//down
                {
                    if(p[x][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==it)
                    {
                        if(p[x][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[x][j]==0)
                            value+=4;
                        if(p[x][j]==0&&(j+2)<=15&&p[x][j+1]==me&&p[x][j+2]!=1)
                            value+=5;
                        break;
                    }
                }
                if(y-1<0)
                    value-=15;
                for(int j=y-1;j>-1;j--)//up
                {
                    if(p[x][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[x][j]==0||p[x][j]==it)
                    {
                        if(p[x][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[x][j]==0)
                            value+=4;
                        if(p[x][j]==0&&(j-2)>0&&p[x][j-1]==me&&p[x][j-2]!=1)
                            value+=5;
                        break;
                    }
                }
                if(q==0&&t>=2)
                    value+=30;
                t=0;q=0;
                if(x+1>=15||y+1>=15)
                    value-=15;
                for(int i=x+1,j=y+1;i<15&&j<15;i++,j++)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][j]==0)
                            value+=4;
                        if(p[i][j]==0&&(j+2)<15&&(i+2)<15&&p[i+1][j+1]==me\
                        &&p[i+2][j+2]!=1)
                            value+=5;
                        break;
                    }
                }
                for(int i=x-1,j=y-1;i>-1&&j>-1;i--,j--)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][j]==0)
                            value+=4;
                        if(p[i][j]==0&&(j-2)>0&&(i-2)>0&&p[i-1][j-1]==me\
                        &&p[i-2][j-2]!=1)
                            value+=5;
                        break;
                    }
                }
                if(q==0&&t>=2)
                    value+=30;
                t=0;q=0;
                if(x-1<0||y+1>=15)
                    value-=15;
                for(int i=x-1,j=y+1;i>-1&&j<15;i--,j++)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][j]==0)
                            value+=4;
                        if(p[i][j]==0&&(j+2)<15&&(i-2)>0&&p[i-1][j+1]==me\
                        &&p[i-2][j+2]!=1)
                            value+=5;
                        break;
                    }
                }
                for(int i=x+1,j=y-1;i<15&&j>-1;i++,j--)//right-down
                {
                    if(p[i][j]==me)
                    {
                        value+=6+8*t;
                        if(t>=2)
                            value+=40;
                        if(t>=3)
                            value+=120;
                        t++;
                    }
                    if(p[i][j]==0||p[i][j]==it)
                    {
                        if(p[i][j]==it&&t>=2)
                        {
                            value-=30;
                            q++;
                            if(q>1)
                                value-=35;
                        }
                        if(p[i][j]==0)
                            value+=4;
                        if(p[i][j]==0&&(j-2)>0&&(i+2)<15&&p[i+1][j-1]==me\
                        &&p[i+2][j-2]!=1)
                            value+=5;
                        break;
                    }
                }
                if(q==0&&t>=2)
                    value+=30;
                t=0;q=0;
                if(value>max)
                {
                    max=value;
                    temp.x=x;temp.y=y;
                    ptemp[0].x=x;
                    ptemp[0].y=y;
                    num=0;
                }
                if(value==max&&num<98)
                {
                    num++;
                    ptemp[num].x=x;
                    ptemp[num].y=y;
                }
                value=0;
                t=0;
            }
        }
    }
    int t=0;
    for(int i=0;i<15;i++)
    {
        for(int j=0;j<15;j++)
        {
            if(p[i][j]!=0)
            {
                t=1;
                break;
            }
        }
        if(t)
            break;
    }
    if(t==0)
    {
        num=0;
        temp.x=7;
        temp.y=7;
    }
    if(num==0)
    {
        p[temp.x][temp.y]=me;
        point.x=temp.x;point.y=temp.y;
    }
    else
    {
        srand((unsigned)time(NULL));
        int s=rand()%(num+1);
        p[ptemp[s].x][ptemp[s].y]=me;
        point.x=ptemp[s].x;point.y=ptemp[s].y;
    }
    free(ptemp);
    return point;
}
