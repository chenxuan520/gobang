#include "mywidget.h"
#include "ui_mywidget.h"
#include<QPainter>
#include<QDebug>
#include<QMouseEvent>
#include<QMessageBox>
MyWidget::MyWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MyWidget)
{
    QStringList list;
    list<<"高级"<<"中级"<<"低级";
    ui->setupUi(this);
    this->setWindowTitle("最强五子棋");
    this->setWindowIcon(QIcon("://五子棋2.0.ico"));
    this->turn=1;
    this->model=0;
    ui->comboBox->addItems(list);
    pai=new AIdown;
    win=0;
    this->resize(313,313);
    for(int i=0;i<15;i++)
        for(int j=0;j<15;j++)
            this->borad[i][j]=0;
}

MyWidget::~MyWidget()
{
    delete ui;
}

void MyWidget::isTurn(int turn)
{
    if(turn==2)
    {
        if(this->model==0)
            this->pai->computerThreeTwo(this->borad,2,this->mouse);
        else if(this->model==1)
            this->pai->computerTry(this->borad,2,this->mouse);
        else
            this->pai->computer(this->borad,2,this->mouse);
    }
    update();
}
void MyWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.drawPixmap(0,0,this->width(),this->height(),QPixmap("://1.jpeg"));
    for(int i=0;i<15;i++)
    {
        for(int j=0;j<15;j++)
        {
            if(this->borad[i][j]==1)
                painter.drawPixmap(20*i+10,20*j+10,15,15,QPixmap(":\\hei1.png"));
            else if(this->borad[i][j]==2)
                painter.drawPixmap(20*i+10,20*j+10,15,15,QPixmap(":\\bai.png"));
        }
    }
}

bool MyWidget::isWin(int one)
{
    if(one==0)
        return false;
    else if (one==1)
    {
        QMessageBox(QMessageBox::NoIcon,"祝贺","你赢了").exec();
        for(int i=0;i<15;i++)
            for(int j=0;j<15;j++)
                this->borad[i][j]=0;
        update();
        this->isTurn(2);
        this->win=0;
    }
    else if (one==2)
    {
        QMessageBox(QMessageBox::NoIcon,"sorry","你输了").exec();
        for(int i=0;i<15;i++)
            for(int j=0;j<15;j++)
                this->borad[i][j]=0;
        update();
        this->win=0;
    }
    return true;
}
void MyWidget::mousePressEvent(QMouseEvent *event)
{
    if((event->x()-15)%20>10)
        this->mouse.x=(event->x()-15)/20+1;
    else
        this->mouse.x=(event->x()-15)/20;
    if((event->y()-15)%20>10)
        this->mouse.y=(event->y()-15)/20+1;
    else
        this->mouse.y=(event->y()-15)/20;
    if(this->pai->peopleDown(this->borad,1,this->mouse))
    {
        this->win=this->pai->judge(this->borad,this->mouse);
        if(this->isWin(this->win))
            return;
        if(this->model==0)
            this->mouse=this->pai->computerThreeTwo(this->borad,2,this->mouse);
        else if(this->model==1)
            this->mouse=this->pai->computerTry(this->borad,2,this->mouse);
        else
            this->mouse=this->pai->computer(this->borad,2,this->mouse);
        update();
        this->win=this->pai->judge(this->borad,this->mouse);
        if(this->isWin(this->win))
            return;
    }
}


void MyWidget::on_comboBox_currentIndexChanged(const QString &)
{
    if(ui->comboBox->currentIndex()==0)
        model=0;
    else if (ui->comboBox->currentIndex()==1)
        model=1;
    else
        model=2;
}



void MyWidget::on_pushButton_clicked()
{
    for(int i=0;i<15;i++)
        for(int j=0;j<15;j++)
            this->borad[i][j]=0;
    update();
}
