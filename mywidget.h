#ifndef MYWIDGET_H
#define MYWIDGET_H
#include"aidown.h"
#include <QWidget>
QT_BEGIN_NAMESPACE
namespace Ui { class MyWidget; }
QT_END_NAMESPACE

class MyWidget : public QWidget
{
    Q_OBJECT

public:
    MyWidget(QWidget *parent = nullptr);
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    bool isWin(int one);
    void isTurn(int turn);
    ~MyWidget();

private slots:


    void on_comboBox_currentIndexChanged(const QString &arg1);

    void on_pushButton_clicked();

private:
    int win;
    int turn;
    int model;
    int borad[15][15];
    Ui::MyWidget *ui;
    AIdown* pai;
    Point mouse;
};
#endif // MYWIDGET_H
