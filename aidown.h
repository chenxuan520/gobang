#ifndef AIDOWN_H
#define AIDOWN_H

struct Point{
    int x;
    int y;
};
class AIdown
{
public:
    AIdown();
    Point computerThree(int (*p)[15],int me,Point pointOne);
    Point computerThreeTwo(int (*p)[15],int me,Point point);
    Point computer(int (*p)[15],int me,Point point);
    Point computerTry(int (*p)[15],int me,Point point);
    int judge(int (*p)[15],Point pointOne);
    bool peopleDown(int (*p)[15],int person,Point point);
};

#endif // AIDOWN_H
